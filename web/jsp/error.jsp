<html lang="es" dir="ltr">

    <head>
        <meta charset="utf-8">
        <title>DAO & DTO</title>

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js"
                integrity="sha512-RXf+QSDCUQs5uwRKaDoXt55jygZZm2V++WUZduaU/Ui/9EGp3f/2KZVahFZBKGH0s774sd3HmrhUy+SgOFQLVQ=="
        crossorigin="anonymous"></script>
        <link rel="stylesheet" href="css/styles.css"/>

    </head>

    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container">
                <div class="navbar-header">
                    <h1>DAO & DTO</h1>
                </div>
            </div>
        </nav>
        <br>
        <!-- Registro -->
        <div class="container">
            <div class="float-end"><a href="index.jsp">
                    <h3><i class="fas fa-home"></i></h3>
            </a></div>
            <%
                int msg = (Integer)request.getSession().getAttribute("id");
            %>
            <h1>Error al crear departamento, Id <%=msg%> ya existe</h1>            
        </div>

        <footer>
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-6 offset-4">
                        <h5>
                            Harold Rueda - 1151904
                            | Jefferson Pallares - 1151508 <br>
                            UFPS-2021-Programación web - UFPS</h5>
                    </div>
                </div>
            </div>
        </footer>

    </body>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf"
    crossorigin="anonymous"></script>

</html>
