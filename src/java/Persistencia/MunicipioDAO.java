/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Persistencia;

import Modelo.Municipios;
import Persistencia.exceptions.NonexistentEntityException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Harold
 */
public class MunicipioDAO {
    MunicipiosJpaController mu = new MunicipiosJpaController();

    public MunicipioDAO() {
    }   
    
    public void create(Municipios m){
        mu.create(m);
    }
    
    public Municipios read(int id){
        return mu.findMunicipios(id);
    }
    
    public List<Municipios> readMun(){
        return mu.findMunicipiosEntities();
    }
    
    public void update(Municipios m){
        try {
            mu.edit(m);
        } catch (Exception ex) {
            Logger.getLogger(MunicipioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void delete(int id){
        try {
            mu.destroy(id);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(MunicipioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
