/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Persistencia;

import Modelo.Departamento;
import Persistencia.exceptions.NonexistentEntityException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Harold
 */
public class DepartamentoDAO {

    DepartamentoJpaController de = new DepartamentoJpaController();

    public DepartamentoDAO() {
    }   
    
    public void create(Departamento d) {
        de.create(d);
    }

    public Departamento read(int id) {
        return de.findDepartamento(id);
    }
    
    public List<Departamento> readDptos(){
        return de.findDepartamentoEntities();
    }
    
    public void update(Departamento d){
        try {
            de.edit(d);
        } catch (Exception ex) {
            Logger.getLogger(DepartamentoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void delete(int id){
        try {
            de.destroy(id);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(DepartamentoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
