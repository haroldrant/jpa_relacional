/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Harold
 */
@Entity
@Table(name = "Departamento")
public class Departamento implements Serializable {

    @Id
    @Column(name = "id_dpto")
    private int id_dpto;
    @Column(name = "nombre")
    private String nombre;

    public Departamento() {
    }

    public Departamento(int id_dpto, String nombre) {
        this.id_dpto = id_dpto;
        this.nombre = nombre;
    }

    public int getId_dpto() {
        return id_dpto;
    }

    public void setId_dpto(int id_dpto) {
        this.id_dpto = id_dpto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
